import os
import numpy as np
import pandas as pd
import string
import nltk
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

nltk.download('stopwords')
nltk.download('averaged_perceptron_tagger')

ps = PorterStemmer()
my_path = os.path.abspath(os.path.dirname(__file__))
spam_data = os.path.join(my_path, "spam.csv")


class clean():
    print("In class")

    # init method or constructor
    # def __init__(self, spam_data):
    #     self.spam_data = spam_data
    def freq(self):
        test = clean.clean_fun(self=set)
        pos_tagging = test.pos_tagging
        print(pos_tagging)

        # break the string into list of words
        str_list = pos_tagging
        print(str_list)
        # gives set of unique words
        unique_words = set(str_list)
        for word in unique_words:
            print('Frequency of ', word, 'is :', str_list.count(word))




    def clean_fun(self):
        with open(spam_data, 'r') as f:
            for item in f:
                words = nltk.word_tokenize(str(item))  # Tokeninzation
                lists = [word.lower() for word in words if word.isalpha()]  # Removing numbersand punctuation
                for word in lists:  # removing stem words
                    without_stem = ps.stem(word)
                    without_stop = word_tokenize(without_stem)
                    tokens_without_sw = [word for word in without_stop if
                                         not word in stopwords.words()]  # stop words removal process
                    self.freq.pos_tagging = nltk.pos_tag(tokens_without_sw)  # POS tagging
                    # print(pos_tagging)
#                     '''Pre-Processing is completed'''
#                     '''Feature selection'''

    # driver code

if __name__ == "__main__":
    clean.freq(self=set)
    # clean.clean_fun(self=set)
    # calling the Cleaning function
