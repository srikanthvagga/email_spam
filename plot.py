import matplotlib.pyplot as plt
import os
import pandas as pd
from collections import Counter

my_path = os.path.abspath(os.path.dirname(__file__))
spam_data = os.path.join(my_path, "last_list.txt")

data = pd.read_csv(spam_data,header = None)
data = data.transpose()
# print(data.columns)
dict_file = data[0].value_counts().to_dict()

# Full data
# print(dict_file)

# Taking only top 10 words
itter = Counter(dict_file)
top_10 = itter.most_common(10)

list_keys = []
list_values = []
# list_dict = dict_file.items()
for i in top_10:
    list_keys.append(i[0]), list_values.append(i[1])

#Appending hard coded values to y-axis
tick_val = [0,50,100,150,200,250,300,350,400,450,500,550,600,650,700,750]
x_axis = []

# Removing extra spaces and quotes
for word in list_keys:
    x = str(word).replace("'","")
    x_axis.append(x)

# Adapt the ticks on the y-axis
plt.yticks(tick_val)
plt.bar(x_axis,list_values)
plt.xlabel("Words")
plt.ylabel("Freq")
plt.title("Top Ten Words")
plt.savefig("word.png")
# plt.show()


