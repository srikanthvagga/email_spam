import matplotlib.pyplot as plt
import os
import pandas as pd

my_path = os.path.abspath(os.path.dirname(__file__))
spam_data = os.path.join(my_path, "last_list.txt")

data = pd.read_csv(spam_data,header = None)
data = data.transpose()
print(data.columns)
dict_file = data[0].value_counts().to_dict()
# print(dict_file)

list_keys = []
list_values = []
list_dict = dict_file.items()
for item in list_dict:
    list_keys.append(item[0]), list_values.append(item[1])
# list_keys = str(list_keys).replace('"','').replace(' ','').replace("[","").replace("]","")
# list_values = str(list_values).replace(" ","")
# print(list_keys)
# print(list_values)
plt.hist(list_keys, list_dict)
plt.title('Word count')
plt.xlabel('Words')
plt.ylabel('Counts')
# plt.xticks(['spam','go'])
# plt.yticks([0,2,4,6,8,10,600,700,800])
plt.show()