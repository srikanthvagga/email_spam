import os
import numpy as np
import pandas as pd
import string
import nltk
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from itertools import count
import collections
import matplotlib.pyplot as plt
import csv
nltk.download('stopwords')
nltk.download('averaged_perceptron_tagger')
ps = PorterStemmer()
my_path = os.path.abspath(os.path.dirname(__file__))
spam_data = os.path.join(my_path, "spam.csv")
# spam_data = os.path.join("spam.csv")
lst = []
def clean_fun():
    with open(spam_data, 'r') as f:
        for item in f:
            # print("Cleaning started")
            words = nltk.word_tokenize(str(item))  # Tokeninzation
            lists = [word.lower() for word in words if word.isalpha()]  # Removing numbers and punctuation
            for word in lists:  # removing stem words
                without_stem = ps.stem(word)
                without_stop = word_tokenize(without_stem)
                tokens_without_sw = [word for word in without_stop if
                                     not word in stopwords.words()]  # stop words removal process
                pos_tagging = nltk.pos_tag(tokens_without_sw)    # POS tagging
                print(pos_tagging)
                # return nltk.pos_tag(tokens_without_sw)
                for tople in pos_tagging:
                    str1 = ''.join(tople[0])
                    str1 = str1 + ','
                    str1 = str1.split()
                    for i in str1:
                        lst.append(i)
                list1 = lst[:-1]
                # print(list1)

            # df = pd.DataFrame(list1, columns=["colummn"])
            # df.to_csv('last_list.csv', index=False)
            # with open("last_list1.csv",'w') as last:
                # last.csv_write(str(lst[:-1]).replace(",'","'"))
                        # plt.hist(lst)
                        # plt.show()
clean_fun()


from test1 import clean_fun
import os
import numpy as np
import matplotlib.pyplot as plt
import collections
count = 0
my_path = os.path.abspath(os.path.dirname(__file__))
read_file = os.path.join(my_path,"last_list.txt")
with open(read_file,'r') as complete_list:
    counts = collections.Counter(complete_list)
    labels, values = zip(*counts.items())
    indSort = np.argsort(values)
    labels = np.array(labels)[indSort]
    values = np.array(values)[indSort]
    indexes = np.arange(len(labels))
    bar_width = 0.35
    plt.bar(indexes,values)
    plt.xticks(indexes + bar_width, labels)
    plt.show()
