import os
import numpy as np
import pandas as pd
import nltk
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from itertools import count

nltk.download('stopwords')
nltk.download('averaged_perceptron_tagger')
ps = PorterStemmer()
my_path = os.path.abspath(os.path.dirname(__file__))
spam_data = os.path.join(my_path, "spam.csv")
stop_words = set(stopwords.words('english'))
def clean_fun():
    with open(spam_data, 'r') as f:
        for item in f:
            words = nltk.word_tokenize(str(item))  # Tokeninzation
            lists = [word.lower() for word in words if word.isalpha()]  # Removing numbers and punctuation
            for word in lists:  # removing stem words
                without_stem = ps.stem(word)
                print(without_stem)
                word_by_word = word_tokenize(without_stem)
                for w in word_by_word:
                    if not w in stop_words:
                        with open("check.txt",'a') as check:
                            check.write(" " + w)

if __name__ == "__main__":
    clean_fun()